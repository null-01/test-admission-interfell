# Admission

### Intro
Swagger documentation: http://localhost:8080/test-admission/v2/api-docs  
Jacoco report unit test: ./test-admission-interfell/test-admission/target/site/jacoco/index.html

### Exercise 2
Una de las estrategias más usadas para cifrar información entre capas es la utilización de tokens y su constante renovación, está estrategia es muy utilizada para generar una llave de accesos temporal con un corto tiempo de vida, permitiendo validar la veracidad del acceso a determinada funcionalidad.

Para el presente ejercicio, podemos considerar un servicio encargado de generar un token, con un tiempo de 30 segundo de vida, este token dentro de su metadata tiene un key-public-secret usado para encoder and decoder cualquier dato que requiera ser ilegible en la transmisión de datos entre las diferentes capas.

Eso quiere decir, que la información encoded solo podrá ser legible si el token logra ser decodificado en su totalidad, ya que la clave necesaria para hacer legible la información transmitida, se encuentra en la metada del token.

Finalmente la única forma de hacer legible la información transmitida, es con un token válido y adicional a eso un key-public-secret válido.



### Exercise 1 - Screenshot API


[![POST](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/swagger-api-rest.png)](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/swagger-api-rest.png)


POST - Create USER
[![POST](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/create-user.png)](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/create-user.png)


GET - Find All USERS
[![POST](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/find-all-users.png)](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/find-all-users.png)


GET - Find By Email USER
[![POST](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/find-by-email-user.png)](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/find-by-email-user.png)


POST - Consume Rest provider
[![POST](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/provider-rest.png)](https://bitbucket.org/null-01/test-admission-interfell/raw/0459c7addf27abd110467859e1055f96006f03c0/docs/screenshot/provider-rest.png)
