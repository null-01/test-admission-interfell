/**
 * 
 */
package com.laboratory.interfell.testadmission.technical;

import java.util.Calendar;
import java.util.Date;

import org.springframework.http.HttpStatus;

/**
 * @author andresduran0205
 *
 */
public class ResponseTechnical {

	private int status;
	private String message;
	private Date timestamp;

	public ResponseTechnical(int status, String message) {
		this.status = status;
		this.message = message;
		this.timestamp = Calendar.getInstance().getTime();
	}

	public ResponseTechnical(HttpStatus httpStatus, String message) {
		this.status = httpStatus.value();
		this.message = message;
		this.timestamp = Calendar.getInstance().getTime();
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
