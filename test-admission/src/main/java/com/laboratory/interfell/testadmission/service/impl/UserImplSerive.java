/**
 * 
 */
package com.laboratory.interfell.testadmission.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.laboratory.interfell.testadmission.domain.User;
import com.laboratory.interfell.testadmission.entity.UserEntity;
import com.laboratory.interfell.testadmission.repository.IUserRepository;
import com.laboratory.interfell.testadmission.service.IUserService;

/**
 * @author andresduran0205
 *
 */
@Service
public class UserImplSerive implements IUserService<User, UserEntity> {

	@Autowired
	private IUserRepository iUserRepository;

	@Override
	public User convertEntityToDomain(UserEntity entity) {
		return User.builder().id(entity.getId()).username(entity.getUsername()).name(entity.getName())
				.email(entity.getEmail()).phone(entity.getPhone()).build();
	}

	@Override
	public UserEntity convertDomainToEntity(User domain) {
		return UserEntity.builder().username(domain.getUsername()).name(domain.getName()).email(domain.getEmail())
				.phone(domain.getPhone()).build();
	}

	@Override
	public Optional<User> create(User domain) {
		UserEntity userEntity = iUserRepository.save(convertDomainToEntity(domain));
		return convert(userEntity);
	}

	@Override
	public Optional<List<User>> findAll() {
		return convert((List) iUserRepository.findAll());
	}

	@Override
	public Optional<List<User>> findByEmail(String email) {
		return convert((List) iUserRepository.findByEmail(email));
	}

}
