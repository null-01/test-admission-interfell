/**
 * 
 */
package com.laboratory.interfell.testadmission.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author andresduran0205
 *
 */

@Entity(name = "USER")
@Data
@Builder
@AllArgsConstructor
public class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String username;
	private String email;
	private Long phone;

	public UserEntity() {
	}

}
