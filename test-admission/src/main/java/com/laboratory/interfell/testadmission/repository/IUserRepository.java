/**
 * 
 */
package com.laboratory.interfell.testadmission.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.laboratory.interfell.testadmission.entity.UserEntity;

/**
 * @author andresduran0205
 *
 */
@Repository
public interface IUserRepository extends CrudRepository<UserEntity, BigInteger> {

	@Transactional(readOnly = true)
	@Query("SELECT p FROM USER p WHERE p.email = :email")
	public List<UserEntity> findByEmail(@Param("email") String email);

}
