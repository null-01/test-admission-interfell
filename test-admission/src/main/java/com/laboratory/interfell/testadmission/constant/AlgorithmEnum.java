/**
 * 
 */
package com.laboratory.interfell.testadmission.constant;

/**
 * @author andres
 *
 */
public enum AlgorithmEnum {

	DECODE_UTF8("UTF8"),

	ENCODE_DES("DES");

	private final String name;

	private AlgorithmEnum(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
