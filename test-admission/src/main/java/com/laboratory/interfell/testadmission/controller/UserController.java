/**
 * 
 */
package com.laboratory.interfell.testadmission.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.laboratory.interfell.testadmission.constant.PathConstant;
import com.laboratory.interfell.testadmission.domain.User;
import com.laboratory.interfell.testadmission.entity.UserEntity;
import com.laboratory.interfell.testadmission.exception.CommonException;
import com.laboratory.interfell.testadmission.exception.CommonException.DATA_NOT_FOUND;
import com.laboratory.interfell.testadmission.exception.CommonException.DATA_NOT_SAVED;
import com.laboratory.interfell.testadmission.service.IUserService;

/**
 * @author andresduran0205
 *
 */

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private IUserService<User, UserEntity> iUserService;

	@PostMapping(value = "/user")
	public Optional<User> create(@RequestBody(required = true) User user) throws DATA_NOT_SAVED {
		Optional<User> created = iUserService.create(user);
		return Optional.ofNullable(created).orElseThrow(CommonException.DATA_NOT_SAVED::new);
	}

	@GetMapping(value = PathConstant.USER_FIND_ALL)
	public Optional<List<User>> findAll() throws DATA_NOT_FOUND {
		return Optional.ofNullable(iUserService.findAll()).orElseThrow(CommonException.DATA_NOT_FOUND::new);

	}

	@GetMapping(value = "/user/template")
	public Optional<List<User>> findBy(@RequestParam String email) throws DATA_NOT_FOUND {
		return Optional.ofNullable(iUserService.findByEmail(email)).orElseThrow(CommonException.DATA_NOT_FOUND::new);

	}

}
