/**
 * 
 */
package com.laboratory.interfell.testadmission.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @author andresduran0205
 *
 */
@Data
@Builder
@ToString
public class User {

	@NotNull
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private String username;
	@NotNull
	private String email;
	@Min(6)
	private Long phone;

}
