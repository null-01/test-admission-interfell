/**
 * 
 */
package com.laboratory.interfell.testadmission.properties;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * @author andresduran0205
 *
 */

@Component
@Data
@ConfigurationProperties(prefix = "backend")
public class ApplicationProperty {
	private Sanbox sanbox;

	@Data
	@ToString
	public static class Sanbox {
		private String secretKey;
		private Api api;

		@Data
		@ToString
		public static class Api {
			private Request searchRut;

			@Data
			@ToString
			public static class Request {
				private String uri;
				private List<String> params;
			}

		}

	}

}
