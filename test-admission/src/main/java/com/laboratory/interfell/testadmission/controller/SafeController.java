/**
 * 
 */
package com.laboratory.interfell.testadmission.controller;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.laboratory.interfell.testadmission.domain.ResponseProvider;
import com.laboratory.interfell.testadmission.domain.ResponseProvider.Result;
import com.laboratory.interfell.testadmission.properties.ApplicationProperty;
import com.laboratory.interfell.testadmission.utility.client.RestfulClientUtility;
import com.laboratory.interfell.testadmission.utility.security.SecurityUtility;

/**
 * @author andresduran0205
 *
 */

@RestController
@RequestMapping("/api")
public class SafeController {

	@Autowired
	private ApplicationProperty applicationProperty;

	@Autowired
	private SecurityUtility securityUtility;

	@PostMapping(value = "/provider")
	public Optional<ResponseProvider> consumeAPI(@RequestParam(value = "rut", required = true) String rut) {
		final String encodeRut = securityUtility.encodeWithDES(applicationProperty.getSanbox().getSecretKey(), rut);

		final RestfulClientUtility clientUtility = new RestfulClientUtility(
				applicationProperty.getSanbox().getApi().getSearchRut().getUri());

		final Map<String, String> querys = clientUtility
				.buildMapQuerys(applicationProperty.getSanbox().getApi().getSearchRut().getParams(), encodeRut);

		final ResponseProvider respone = clientUtility.get(querys).commit(new TypeReference<ResponseProvider>() {
		});

		if (Optional.ofNullable(respone).isPresent())
			respone.setElapsedTime(clientUtility.getRequestTime());

		if (Optional.ofNullable(respone).map(ResponseProvider::getResult).map(Result::getItems).isPresent()) {
			respone.getResult().setRegisterCount(respone.getResult().getItems().size());
			respone.getResult().setItems(null);
		}

		return Optional.of(respone);
	}

}
