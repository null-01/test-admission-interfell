/**
 * 
 */
package com.laboratory.interfell.testadmission.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author andres
 *
 */

public interface ICommonService<D, E> {

	Optional<D> create(D domain);

	Optional<List<D>> findAll();

	D convertEntityToDomain(E entity);

	E convertDomainToEntity(D domain);

	default Optional<D> convert(E entity) {
		return Optional.of(convertEntityToDomain(entity));
	}

	default Optional<List<D>> convert(List<E> entitys) {
		return Optional.of(entitys.stream().map(entity -> convertEntityToDomain(entity)).collect(Collectors.toList()));
	}

	Object findByEmail(String email);

}
