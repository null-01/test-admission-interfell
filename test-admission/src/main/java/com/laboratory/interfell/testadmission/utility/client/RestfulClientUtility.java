/**
 * 
 */
package com.laboratory.interfell.testadmission.utility.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author andresduran0205
 *
 */

@Slf4j
public class RestfulClientUtility {

	private final String uri;
	private String response;
	private long requestTime;

	public RestfulClientUtility(final String uri) {
		this.uri = uri;
	}

	private UriComponentsBuilder configuration(Map<String, Object> paths, Map<String, String> querys) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(this.uri);
		if (querys != null && !querys.isEmpty())
			for (Map.Entry<String, String> entry : querys.entrySet())
				builder.queryParam(entry.getKey(), entry.getValue());
		if (paths != null && !paths.isEmpty())
			builder.uriVariables(paths);
		log.info("URI_REQUEST [{}]", builder.build().toUri());
		return builder;
	}

	private RestfulClientUtility get(MultiValueMap<String, String> httpHeaders, Map<String, Object> paths,
			Map<String, String> querys) {
		HttpHeaders headers = new HttpHeaders();
		if (httpHeaders != null)
			headers.addAll(httpHeaders);
		UriComponentsBuilder builder = configuration(paths, querys);
		this.requestTime = System.currentTimeMillis();
		ResponseEntity<String> responseEntity = new RestTemplate().exchange(builder.build().toUri(), HttpMethod.GET,
				new HttpEntity<String>(headers), String.class);
		this.response = responseEntity.getBody();
		this.requestTime = System.currentTimeMillis() - this.requestTime;
		log.info("RESPONSE_HTTP_HEAD_GET [{}]", responseEntity);
		return this;
	}

	public RestfulClientUtility get(Map<String, String> querys) {
		return get(null, null, querys);
	}

	public <T> T commit(TypeReference<T> typeReference) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.setSerializationInclusion(Include.NON_NULL);
		T entity = null;
		try {
			entity = mapper.readValue(this.response, typeReference);
		} catch (JsonParseException e) {
			log.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return entity;
	}

	public Map<String, String> buildMapQuerys(List<String> keys, Object... values) {
		Map<String, String> querys = new HashMap<String, String>();
		if (Optional.ofNullable(keys).map($ -> !$.isEmpty()).isPresent())
			for (int index = 0; index < values.length; index++)
				querys.put(keys.get(index), String.valueOf(values[index]));
		return querys;
	}

	public String getUri() {
		return uri;
	}

	public long getRequestTime() {
		return requestTime;
	}

	public String getResponse() {
		return response;
	}

}
