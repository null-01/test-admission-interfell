/**
 * 
 */
package com.laboratory.interfell.testadmission.utility.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.springframework.stereotype.Component;

import com.laboratory.interfell.testadmission.constant.AlgorithmEnum;

import lombok.extern.slf4j.Slf4j;

/**
 * @author andresduran0205
 *
 */

@Slf4j
@Component
public class SecurityUtility {

	public String encodeWithDES(final String key, final String text) {
		String encode = "";
		try {
			DESKeySpec desKeySpec = new DESKeySpec(key.getBytes(AlgorithmEnum.DECODE_UTF8.getName()));
			SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(AlgorithmEnum.ENCODE_DES.getName());

			SecretKey secretKey = secretKeyFactory.generateSecret(desKeySpec);

			Cipher cipher = Cipher.getInstance(AlgorithmEnum.ENCODE_DES.getName());
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			
			final byte[] encryptedBytes = cipher.doFinal(text.getBytes(AlgorithmEnum.DECODE_UTF8.getName()));
			encode = new String(Base64.getEncoder().encode(encryptedBytes));

		} catch (InvalidKeyException e) {
			log.error(e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage(), e);
		} catch (NoSuchPaddingException e) {
			log.error(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalBlockSizeException e) {
			log.error(e.getMessage(), e);
		} catch (BadPaddingException e) {
			log.error(e.getMessage(), e);
		} catch (InvalidKeySpecException e) {
			log.error(e.getMessage(), e);
		}
		return encode;
	}

}
