/**
 * 
 */
package com.laboratory.interfell.testadmission.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @author andresduran0205
 *
 */
@Data
@Builder
@AllArgsConstructor
@ToString
public class ResponseProvider {

	private Integer responseCode;
	private String description;
	private Long elapsedTime;
	private Result result;

	public ResponseProvider() {
	}

	@Data
	@Builder
	@AllArgsConstructor
	@ToString
	public static class Result {
		private Integer registerCount;
		private List<Object> items;

		public Result() {
		}
	}

}
