/**
 * 
 */
package com.laboratory.interfell.testadmission.service;

import java.util.List;
import java.util.Optional;

import com.laboratory.interfell.testadmission.domain.User;

/**
 * @author andresduran0205
 *
 */

public interface IUserService<D, E> extends ICommonService<D, E> {

	Optional<List<User>> findByEmail(String email);

}
