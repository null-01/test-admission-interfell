/**
 * 
 */
package com.laboratory.interfell.testadmission.exception;

/**
 * @author andresduran0205
 *
 */


public class CommonException {

	public static class DATA_NOT_FOUND extends Exception {

	}

	public static class DATA_NOT_SAVED extends Exception {

	}

	
}
