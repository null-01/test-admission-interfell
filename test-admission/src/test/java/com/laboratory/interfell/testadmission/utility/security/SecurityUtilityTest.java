/**
 * 
 */
package com.laboratory.interfell.testadmission.utility.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import com.laboratory.interfell.testadmission.properties.ApplicationProperty;
import com.laboratory.interfell.testadmission.utility.DomainFactory;

/**
 * @author andres
 *
 */
@SpringBootTest(classes = { ApplicationProperty.class })
@EnableConfigurationProperties
@ExtendWith(MockitoExtension.class)
public class SecurityUtilityTest {

	private String SECRET_KEY;

	@Autowired
	private ApplicationProperty applicationProperty;

	@InjectMocks
	private SecurityUtility securityUtilityMock;

	@BeforeEach
	void setMockOutput() {
		this.SECRET_KEY = applicationProperty.getSanbox().getSecretKey();
	}

	@Test
	@DisplayName("Utility Security - success encode DES")
	public void utilityEncodeDESSuccess() throws Exception {
		final String TEXT = DomainFactory.RUT_SUCCESS;
		final String ENCODE_STR = securityUtilityMock.encodeWithDES(SECRET_KEY, TEXT);
		Assertions.assertEquals(ENCODE_STR, DomainFactory.ENCODE_RUT_SUCCESS);
	}

	@Test
	@DisplayName("Utility Security - wrong encode DES")
	public void utilityEncodeDESWrong() throws Exception {
		final String TEXT = DomainFactory.RUT_WRONG;
		final String ENCODE_STR = securityUtilityMock.encodeWithDES(SECRET_KEY, TEXT);
		Assertions.assertNotEquals(ENCODE_STR, DomainFactory.ENCODE_RUT_WRONG);
	}

}
