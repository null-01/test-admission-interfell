/**
 * 
 */
package com.laboratory.interfell.testadmission.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.laboratory.interfell.testadmission.domain.User;
import com.laboratory.interfell.testadmission.entity.UserEntity;
import com.laboratory.interfell.testadmission.repository.IUserRepository;
import com.laboratory.interfell.testadmission.service.impl.UserImplSerive;
import com.laboratory.interfell.testadmission.utility.DomainFactory;

/**
 * @author andresduran0205
 *
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	@Mock
	private IUserRepository iUserRepositoryMock;

	@InjectMocks
	private UserImplSerive userImplSeriveMock;

	@Test
	@DisplayName("Create user - success")
	public void createUserSuccess() throws Exception {
		Mockito.when(iUserRepositoryMock.save(Mockito.any(UserEntity.class))).thenReturn(DomainFactory.getUserEntity());
		Optional<User> user = userImplSeriveMock.create(DomainFactory.getUser().get());
		Assertions.assertEquals(user, DomainFactory.getUser());
	}

	@Test
	@DisplayName("Find all users - success")
	public void findAllUsersSuccess() throws Exception {
		Mockito.when(iUserRepositoryMock.findAll()).thenReturn(DomainFactory.getUsersEntity());
		Optional<List<User>> users = userImplSeriveMock.findAll();
		Assertions.assertEquals(users, DomainFactory.getUsers());
	}

	@Test
	@DisplayName("Find user by email - success")
	public void findUserByEmailSuccess() throws Exception {
		Mockito.when(iUserRepositoryMock.findByEmail(DomainFactory.EMAIL_SUCCESS))
				.thenReturn(DomainFactory.getUsersEntity());
		Optional<List<User>> users = userImplSeriveMock.findByEmail(DomainFactory.EMAIL_SUCCESS);
		Assertions.assertEquals(users, DomainFactory.getUsers());
	}

	@Test
	@DisplayName("Find user by email - empty data")
	public void findUserByEmailEmptyData() throws Exception {
		Mockito.when(iUserRepositoryMock.findByEmail(DomainFactory.EMAIL_WRONG)).thenReturn(Mockito.anyList());
		Optional<List<User>> users = userImplSeriveMock.findByEmail(DomainFactory.EMAIL_WRONG);
		Assertions.assertEquals(users, Optional.of(new ArrayList<User>()));
	}

}
