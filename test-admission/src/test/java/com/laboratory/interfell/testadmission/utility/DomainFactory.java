/**
 * 
 */
package com.laboratory.interfell.testadmission.utility;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.laboratory.interfell.testadmission.domain.ResponseProvider;
import com.laboratory.interfell.testadmission.domain.User;
import com.laboratory.interfell.testadmission.domain.ResponseProvider.Result;
import com.laboratory.interfell.testadmission.entity.UserEntity;

/**
 * @author andresduran0205
 *
 */

public class DomainFactory {

	public static final Long ID_SUCCESS = BigInteger.TEN.longValue();
	public static final String NAME_SUCCESS = "some";
	public static final String USERNAME_SUCCESS = "somothink";
	public static final String EMAIL_SUCCESS = "email@gmail.com";
	public static final String EMAIL_WRONG = "email_wrong@gmail.com";
	public static final long PHONE_SUCCESS = 300123005;
	
	public static final String RUT_SUCCESS = "1-9";
	public static final String RUT_WRONG = "000";
	
	
	public static final Object ENCODE_RUT_SUCCESS = "FyaSTkGi8So=";
	public static final Object ENCODE_RUT_WRONG = "sE89198DSFRTYRT87Ysdf4rthtkiu94";	

	public static UserEntity getUserEntity() {
		return UserEntity.builder().id(ID_SUCCESS).name(NAME_SUCCESS).username(USERNAME_SUCCESS).email(EMAIL_SUCCESS)
				.phone(PHONE_SUCCESS).build();
	}

	public static Optional<User> getUser() {
		return Optional.of(User.builder().id(ID_SUCCESS).name(NAME_SUCCESS).username(USERNAME_SUCCESS).email(EMAIL_SUCCESS)
				.phone(PHONE_SUCCESS).build());
	}

	public static Optional<List<User>> getUsers() {
		List<User> users = new ArrayList<User>();
		users.add(getUser().get());
		users.add(getUser().get());
		users.add(getUser().get());
		return Optional.of(users);
	}

	public static List<UserEntity> getUsersEntity() {
		List<UserEntity> users = new ArrayList<UserEntity>();
		users.add(getUserEntity());
		users.add(getUserEntity());
		users.add(getUserEntity());
		return users;
	}

	public static Optional<ResponseProvider> getResponseProvider() {
		return Optional.of(ResponseProvider.builder().responseCode(BigInteger.ZERO.intValue()).description("OK")
				.result(Result.builder().registerCount(BigInteger.ZERO.intValue()).build()).build());
	}

}
