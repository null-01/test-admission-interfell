/**
 * 
 */
package com.laboratory.interfell.testadmission.controller;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import com.laboratory.interfell.testadmission.domain.ResponseProvider;
import com.laboratory.interfell.testadmission.properties.ApplicationProperty;
import com.laboratory.interfell.testadmission.utility.DomainFactory;
import com.laboratory.interfell.testadmission.utility.security.SecurityUtility;

/**
 * @author andresduran0205
 *
 */

@SpringBootTest(classes = { ApplicationProperty.class })
@EnableConfigurationProperties
@ExtendWith(MockitoExtension.class)
public class SafeControllerTest {

	@Autowired
	private ApplicationProperty applicationProperty;

	@InjectMocks
	private SafeController safeControllerMock;

	@Mock
	private SecurityUtility securityUtilityMock;

	@Mock
	private ApplicationProperty applicationPropertyMock;

	@BeforeEach
	void setMockOutput() {
		Mockito.when(applicationPropertyMock.getSanbox()).thenReturn(applicationProperty.getSanbox());
	}

	@Test
	@DisplayName("Consume API Provider - success rut")
	public void consumeProviderSuccessParam() throws Exception {
		Optional<ResponseProvider> responseExpected = safeControllerMock.consumeAPI(DomainFactory.RUT_SUCCESS);

		Optional<ResponseProvider> responseActual = DomainFactory.getResponseProvider();
		responseExpected.get().setElapsedTime(responseActual.get().getElapsedTime());

		Assertions.assertEquals(responseExpected, responseActual);
	}

}
