/**
 * 
 */
package com.laboratory.interfell.testadmission.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.laboratory.interfell.testadmission.domain.User;
import com.laboratory.interfell.testadmission.exception.CommonException.DATA_NOT_FOUND;
import com.laboratory.interfell.testadmission.exception.CommonException.DATA_NOT_SAVED;
import com.laboratory.interfell.testadmission.service.impl.UserImplSerive;
import com.laboratory.interfell.testadmission.utility.DomainFactory;

/**
 * @author andresduran0205
 *
 */

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

	@InjectMocks
	private UserController userControllerMock;

	@Mock
	private UserImplSerive userImplSeriveMock;

	@Test
	@DisplayName("Create user - success")
	public void createUserSuccess() throws DATA_NOT_SAVED {
		Mockito.when(userImplSeriveMock.create(Mockito.any(User.class))).thenReturn(DomainFactory.getUser());
		Optional<User> userExpected = userControllerMock.create(DomainFactory.getUser().get());
		Assertions.assertEquals(userExpected, DomainFactory.getUser());
	}

	@Test
	@DisplayName("Find all users - success")
	public void findAllUsersSuccess() throws DATA_NOT_FOUND {
		Mockito.when(userImplSeriveMock.findAll()).thenReturn(DomainFactory.getUsers());
		Optional<List<User>> userExpected = userControllerMock.findAll();
		Assertions.assertEquals(userExpected, DomainFactory.getUsers());
	}

	@Test
	@DisplayName("Find user by email - success")
	public void findUserByEmailSuccess() throws DATA_NOT_FOUND {
		Mockito.when(userImplSeriveMock.findByEmail(Mockito.anyString())).thenReturn(DomainFactory.getUsers());
		Optional<List<User>> userExpected = userControllerMock.findBy(DomainFactory.EMAIL_SUCCESS);
		Assertions.assertEquals(userExpected, DomainFactory.getUsers());
	}

	@Test
	@DisplayName("Find user by email - empty")
	public void findUserByEmailEmptyResponse() throws DATA_NOT_FOUND {
		final Optional<List<User>> empty = Optional.of(new ArrayList<User>());
		Mockito.when(userImplSeriveMock.findByEmail(Mockito.anyString())).thenReturn(empty);
		Optional<List<User>> userExpected = userControllerMock.findBy(DomainFactory.EMAIL_WRONG);
		Assertions.assertEquals(userExpected, empty);
	}

}
